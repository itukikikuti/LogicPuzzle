﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace windblow.Drangeon
{
    public class SelectMenu : MonoBehaviour
    {
        [SerializeField] private RectTransform view = null;
        [SerializeField] private GameObject selectButtonPrefab = null;
        private string[] levelNames = new string[]
        {
            "プログラムは上から順に",
            "変数は中身が変わる",
            "足し引き掛け割り余り",
            "加算減算乗算除算剰余",
            "負の数値",
            "プラスはマイナスに マイナスはプラスに",
            "プラスプラス",
            "真か偽か",
            "数値が同じか比べる",
            "数値の大小を比べる",
            "条件分岐1",
            "条件分岐2",
            "条件分岐3",
            "条件分岐4",
            "条件分岐5",
            "条件分岐6",
        };

        private void Start()
        {
            view.sizeDelta = new Vector2(0f, 50f * (levelNames.Length + 1));

            for (int i = 0; i < levelNames.Length; i++)
            {
                CreateButton(levelNames[i], "Level" + i);
            }

            CreateButton("エンドレスモード", "Endless");
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                SceneManager.LoadScene("Test");
            }
        }

        private void CreateButton(string levelName, string sceneName)
        {
            GameObject clone = Instantiate(selectButtonPrefab, view);
            clone.GetComponent<Button>().onClick.AddListener(() => { SceneManager.LoadScene(sceneName); });
            clone.GetComponentInChildren<Text>().text = levelName;
        }
    }
}
