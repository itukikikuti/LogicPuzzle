﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using DG.Tweening;

namespace windblow.Drangeon
{
    public class Player : Character
    {
        [SerializeField] public float speed = 5f;
        [SerializeField] private Transform bodyParent = null;
        [SerializeField] private AudioClip stepSound = null;
        [SerializeField] private Transform leftHand = null;
        [SerializeField] private Transform rightHand = null;
        [SerializeField] private Transform leftFoot = null;
        [SerializeField] private Transform rightFoot = null;

        private float animationTime = 0f;
        private int footIndex = 0;

        protected override void Update()
        {
            base.Update();

            Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, transform.position + new Vector3(0f, 7f, 0f), 0.3f);

            velocity += new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical")).normalized * speed;

            if (Input.GetAxis("Horizontal") != 0f || Input.GetAxis("Vertical") != 0f)
            {
                bodyParent.localRotation = Quaternion.Lerp(bodyParent.localRotation, Quaternion.LookRotation(new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"))), 0.5f);

                animationTime += Time.deltaTime * 20f;
                animationTime %= Mathf.PI * 2f;
                leftHand.localPosition = new Vector3(-0.4f, -0.15f, 0f) + GetHandPosition(animationTime, new Vector3(0.3f, 0.3f, 0.3f));
                rightHand.localPosition = new Vector3(0.4f, -0.15f, 0f) + GetHandPosition(animationTime + Mathf.PI, new Vector3(-0.3f, 0.3f, 0.3f));
                leftFoot.localPosition = new Vector3(-0.15f, -0.4f, 0f) + GetFootPosition(animationTime, new Vector3(0.3f, 0.3f, 0.3f));
                rightFoot.localPosition = new Vector3(0.15f, -0.4f, 0f) + GetFootPosition(animationTime + Mathf.PI, new Vector3(0.3f, 0.3f, 0.3f));

                if (footIndex == 0)
                {
                    if (animationTime < Mathf.PI)
                    {
                        footIndex = 1;
                        AudioSource.PlayClipAtPoint(stepSound, transform.position);
                    }
                }
                else
                {
                    if (animationTime > Mathf.PI)
                    {
                        footIndex = 0;
                        AudioSource.PlayClipAtPoint(stepSound, transform.position);
                    }
                }
            }
            else
            {
                leftHand.localPosition = new Vector3(-0.4f, -0.15f, 0f);
                rightHand.localPosition = new Vector3(0.4f, -0.15f, 0f);
                leftFoot.localPosition = new Vector3(-0.15f, -0.4f, 0f);
                rightFoot.localPosition = new Vector3(0.15f, -0.4f, 0f);
            }
        }

        public Vector3 GetHandPosition(float t, Vector3 scale)
        {
            return new Vector3(
                Mathf.Abs(Mathf.Sin(t)) * 0.5f * scale.x,
                Mathf.Abs(Mathf.Sin(t)) * 0.5f * scale.y,
                -Mathf.Sin(t) * scale.z
            );
        }

        public Vector3 GetFootPosition(float t, Vector3 scale)
        {
            return new Vector3(
                0f,
                Mathf.Max(0f, Mathf.Cos(t)) * 0.5f * scale.y,
                Mathf.Sin(t) * scale.z
            );
        }
    }
}
