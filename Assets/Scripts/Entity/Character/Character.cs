﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    [RequireComponent(typeof(Controller))]
    public class Character : Entity
    {
        protected Controller characterController;
        protected Vector3 velocity = Vector3.zero;
        private bool initialized = false;

        protected virtual void Start()
        {
            characterController = GetComponent<Controller>();
        }

        protected virtual void Update()
        {
            // Updateを1フレーム待たないとCharacterControllerがバグってどこかに吹っ飛ぶ。
            if (!initialized)
            {
                initialized = true;
                return;
            }

            velocity *= 0.5f;
            characterController.SimpleMove(velocity);
        }
    }
}
