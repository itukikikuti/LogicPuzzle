﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public class Beep : Block
    {
        public static int callCount = 0;

        [SerializeField] private AudioClip beepSound = null;

        public void PlaySound()
        {
            AudioSource.PlayClipAtPoint(beepSound, transform.position);
        }
    }
}
