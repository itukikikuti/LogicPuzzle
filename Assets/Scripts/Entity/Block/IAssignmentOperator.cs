﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public interface IAssignmentOperator
    {
        void Assign<T>(IValue<T> variable, T? value) where T : struct;
    }
}
