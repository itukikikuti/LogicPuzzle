﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public class BoolVariable : Block, IValue<bool>, IVariable
    {
        public string Name { get; set; }
        public IVariableDefine Define { get; set; }

        public bool? Value
        {
            get => Define.Value as bool?;
            set => Define.Value = value;
        }

        object IValue.Value
        {
            get => Define.Value;
            set => Define.Value = value;
        }

        protected override void Update()
        {
            base.Update();

            text.text = Name;
        }
    }
}
