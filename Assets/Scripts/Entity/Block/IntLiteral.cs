﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public class IntLiteral : Block, IValue<int>
    {
        [SerializeField] private int value = default;

        public int? Value
        {
            get => value;
            set => this.value = value ?? default;
        }

        object IValue.Value
        {
            get => value;
            set => this.value = value as int? ?? default;
        }

        protected override void Update()
        {
            base.Update();

            text.text = value.ToString();
        }
    }
}
