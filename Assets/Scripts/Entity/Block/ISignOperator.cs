﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public interface ISignOperator
    {
        int? Sign(int? value);
    }
}
