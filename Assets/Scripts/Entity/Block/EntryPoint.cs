﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace windblow.Drangeon
{
    public class EntryPoint : Define
    {
        [HideInInspector] public int beepCallCount = 0;
        [HideInInspector] public int wait = 500;
        [SerializeField] private Transform point = null;

        public async void Run()
        {
            Initialize();

            isRunning = true;

            Vector3 position = transform.position;

            Block block;
            while (true)
            {
                position += Vector3.back;
                block = GetBlock(position);

                if (block == null)
                    break;

                block.isRunning = true;

                if (block is Beep)
                {
                    await ProcessBeep(position, block as Beep);
                }
                if (block is IntVariable)
                {
                    await ProcessInt(position, block as IntVariable);
                }
                if (block is BoolVariable)
                {
                    await ProcessBool(position, block as BoolVariable);
                }
                if (block is If)
                {
                    position = await ProcessIf(position, block as If);
                }
                if (block is Else)
                {
                    position = await ProcessElse(position, block as Else);
                }

                block.isRunning = false;
            }

            isRunning = false;
        }

        private void Initialize()
        {
            beepCallCount = 0;
        }

        private async Task ProcessBeep(Vector3 position, Beep beep)
        {
            beep.PlaySound();
            await Task.Delay(1000);
            beepCallCount++;
        }

        private List<Block> CollectRightBlocks(Vector3 position)
        {
            List<Block> blocks = new List<Block>();
            Vector3 temp = position;

            while (true)
            {
                temp += Vector3.right;
                Block block = GetBlock(temp);

                if (block == null)
                    break;

                blocks.Add(block);
            }

            return blocks;
        }

        private async Task ProcessInt(Vector3 position, IntVariable variable)
        {
            await Task.Delay(wait);

            List<Block> blocks = CollectRightBlocks(position);

            await ProcessAssign(variable, blocks);
            await ProcessAssignBySign(variable, blocks);
            await ProcessIncrementAndDecrement(variable, blocks);
        }

        private async Task ProcessBool(Vector3 position, BoolVariable variable)
        {
            await Task.Delay(wait);

            List<Block> blocks = CollectRightBlocks(position);

            await ProcessBoolAssign(variable, blocks);
            await ProcessBoolAssignByCompare(variable, blocks);
        }

        private async Task<Vector3> ProcessIf(Vector3 position, If ifStatement)
        {
            await Task.Delay(wait);

            List<Block> blocks = CollectRightBlocks(position);

            position = await ProcessIfByBool(position, ifStatement, blocks);
            position = await ProcessIfByCompare(position, ifStatement, blocks);

            return position;
        }

        private async Task<Vector3> ProcessElse(Vector3 position, Else elseStatement)
        {
            await Task.Delay(wait);

            List<Block> blocks = CollectRightBlocks(position);

            if (elseStatement.ifStatement.Value == true)
            {
                if (elseStatement.ifStatement.endIfStatement != null)
                {
                    position = elseStatement.ifStatement.endIfStatement.transform.position;
                    Vector3 start = elseStatement.transform.position + new Vector3(-0.5f, 0f, 0f);
                    Vector3 end = elseStatement.ifStatement.endIfStatement.transform.position + new Vector3(-0.5f, 0f, 0f);
                    StartCoroutine(MovePointBezier(start, (start + end) / 2f + new Vector3(-1f, 0f, 0f), end, wait * 2));
                    await Task.Delay(wait * 2);
                }
            }

            return position;
        }

        private IEnumerator MovePointBezier(Vector3 a, Vector3 control, Vector3 b, int milliSecond)
        {
            point.gameObject.SetActive(false);
            point.position = a;
            point.gameObject.SetActive(true);

            float second = milliSecond / 1000f;
            float t = 0f;
            while (t < second)
            {
                point.position = Utility.Bezier(a, control, b, t / second);
                t += Time.deltaTime;
                yield return null;
            }

            point.position = b;
        }

        private async Task ProcessAssign(IntVariable variable, List<Block> blocks)
        {
            if (blocks.Count >= 2 &&
                blocks[0] is IAssignmentOperator assign &&
                blocks[1] is IValue<int> int1)
            {
                blocks[0].isRunning = true;
                await Task.Delay(wait);
                blocks[1].isRunning = true;
                await Task.Delay(wait);

                if (blocks.Count >= 4 &&
                    blocks[2] is IComputeOperator compute &&
                    blocks[3] is IValue<int> int2)
                {
                    blocks[2].isRunning = true;
                    await Task.Delay(wait);
                    blocks[3].isRunning = true;
                    await Task.Delay(wait);

                    assign.Assign(variable, compute.Compute(int1.Value, int2.Value));

                    blocks[2].isRunning = false;
                    blocks[3].isRunning = false;
                }
                else
                {
                    assign.Assign(variable, int1.Value);
                }

                blocks[0].isRunning = false;
                blocks[1].isRunning = false;
            }
        }

        private async Task ProcessBoolAssign(BoolVariable variable, List<Block> blocks)
        {
            if (blocks.Count >= 2 &&
                blocks[0] is IAssignmentOperator assign &&
                blocks[1] is IValue<bool> bool1)
            {
                blocks[0].isRunning = true;
                await Task.Delay(wait);
                blocks[1].isRunning = true;
                await Task.Delay(wait);

                assign.Assign(variable, bool1.Value);

                blocks[0].isRunning = false;
                blocks[1].isRunning = false;
            }
        }

        private async Task ProcessBoolAssignByCompare(BoolVariable variable, List<Block> blocks)
        {
            if (blocks.Count >= 4 &&
                blocks[0] is IAssignmentOperator assign &&
                blocks[1] is IValue<int> int1 &&
                blocks[2] is ICompareOperator compare &&
                blocks[3] is IValue<int> int2)
            {
                blocks[0].isRunning = true;
                await Task.Delay(wait);
                blocks[1].isRunning = true;
                await Task.Delay(wait);
                blocks[2].isRunning = true;
                await Task.Delay(wait);
                blocks[3].isRunning = true;
                await Task.Delay(wait);

                assign.Assign(variable, compare.Compare(int1.Value, int2.Value));

                blocks[0].isRunning = false;
                blocks[1].isRunning = false;
                blocks[2].isRunning = false;
                blocks[3].isRunning = false;
            }
        }

        private async Task ProcessAssignBySign(IntVariable variable, List<Block> blocks)
        {
            if (blocks.Count >= 3 &&
                blocks[0] is IAssignmentOperator assign &&
                blocks[1] is ISignOperator sign &&
                blocks[2] is IValue<int> value)
            {
                blocks[0].isRunning = true;
                await Task.Delay(wait);
                blocks[1].isRunning = true;
                await Task.Delay(wait);
                blocks[2].isRunning = true;
                await Task.Delay(wait);

                assign.Assign(variable, sign.Sign(value.Value));

                blocks[0].isRunning = false;
                blocks[1].isRunning = false;
                blocks[2].isRunning = false;
            }
        }

        private async Task ProcessIncrementAndDecrement(IntVariable variable, List<Block> blocks)
        {
            if (blocks.Count >= 1 &&
                (blocks[0] is Increment || blocks[0] is Decrement))
            {
                blocks[0].isRunning = true;
                await Task.Delay(wait);

                IAssignmentOperator assign = blocks[0] as IAssignmentOperator;
                assign.Assign(variable, null);

                blocks[0].isRunning = false;
            }
        }

        private async Task<Vector3> ProcessIfByBool(Vector3 position, If ifStatement, List<Block> blocks)
        {
            if (blocks.Count >= 1 &&
                blocks[0] is IValue<bool> bool1)
            {
                blocks[0].isRunning = true;
                await Task.Delay(wait);

                ifStatement.Value = bool1.Value;
                ifStatement.CollectOtherStatement();

                if (bool1.Value == false)
                {
                    if (ifStatement.elseStatement != null)
                    {
                        position = ifStatement.elseStatement.transform.position;
                        Vector3 start = ifStatement.transform.position + new Vector3(-0.5f, 0f, 0f);
                        Vector3 end = ifStatement.elseStatement.transform.position + new Vector3(-0.5f, 0f, 0f);
                        StartCoroutine(MovePointBezier(start, (start + end) / 2f + new Vector3(-1f, 0f, 0f), end, wait * 2));
                        await Task.Delay(wait * 2);
                    }
                    else if (ifStatement.endIfStatement != null)
                    {
                        position = ifStatement.endIfStatement.transform.position;
                        Vector3 start = ifStatement.transform.position + new Vector3(-0.5f, 0f, 0f);
                        Vector3 end = ifStatement.endIfStatement.transform.position + new Vector3(-0.5f, 0f, 0f);
                        StartCoroutine(MovePointBezier(start, (start + end) / 2f + new Vector3(-1f, 0f, 0f), end, wait * 2));
                        await Task.Delay(wait * 2);
                    }
                }

                blocks[0].isRunning = false;
            }

            return position;
        }

        private async Task<Vector3> ProcessIfByCompare(Vector3 position, If ifStatement, List<Block> blocks)
        {
            if (blocks.Count >= 3 &&
                blocks[0] is IValue<int> int1 &&
                blocks[1] is ICompareOperator compare &&
                blocks[2] is IValue<int> int2)
            {
                blocks[0].isRunning = true;
                await Task.Delay(wait);
                blocks[1].isRunning = true;
                await Task.Delay(wait);
                blocks[2].isRunning = true;
                await Task.Delay(wait);

                ifStatement.Value = compare.Compare(int1.Value, int2.Value);
                ifStatement.CollectOtherStatement();

                if (ifStatement.Value == false)
                {
                    if (ifStatement.elseStatement != null)
                    {
                        position = ifStatement.elseStatement.transform.position;
                        Vector3 start = ifStatement.transform.position + new Vector3(-0.5f, 0f, 0f);
                        Vector3 end = ifStatement.elseStatement.transform.position + new Vector3(-0.5f, 0f, 0f);
                        StartCoroutine(MovePointBezier(start, (start + end) / 2f + new Vector3(-1f, 0f, 0f), end, wait * 2));
                        await Task.Delay(wait * 2);
                    }
                    else if (ifStatement.endIfStatement != null)
                    {
                        position = ifStatement.endIfStatement.transform.position;
                        Vector3 start = ifStatement.transform.position + new Vector3(-0.5f, 0f, 0f);
                        Vector3 end = ifStatement.endIfStatement.transform.position + new Vector3(-0.5f, 0f, 0f);
                        StartCoroutine(MovePointBezier(start, (start + end) / 2f + new Vector3(-1f, 0f, 0f), end, wait * 2));
                        await Task.Delay(wait * 2);
                    }
                }

                blocks[0].isRunning = false;
                blocks[1].isRunning = false;
                blocks[2].isRunning = false;
            }

            return position;
        }
    }
}
