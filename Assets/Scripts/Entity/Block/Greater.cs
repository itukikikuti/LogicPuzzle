﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public class Greater : Block, ICompareOperator
    {
        public bool? Compare(int? a, int? b)
        {
            return a > b;
        }
    }
}
