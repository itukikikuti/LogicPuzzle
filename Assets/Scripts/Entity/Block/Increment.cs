﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public class Increment : Block, IAssignmentOperator
    {
        public void Assign<T>(IValue<T> variable, T? value) where T : struct
        {
            if (variable is IValue<int> temp)
                temp.Value++;
        }
    }
}
