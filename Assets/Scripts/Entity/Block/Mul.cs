﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public class Mul : Block, IComputeOperator
    {
        public int? Compute(int? a, int? b)
        {
            return a * b;
        }
    }
}
