﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public interface IVariable : IValue
    {
        string Name { get; set; }
        IVariableDefine Define { get; set; }
    }
}
