﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public class Sub : Block, IComputeOperator, ISignOperator
    {
        public int? Compute(int? a, int? b)
        {
            return a - b;
        }

        public int? Sign(int? value)
        {
            return -value;
        }
    }
}
