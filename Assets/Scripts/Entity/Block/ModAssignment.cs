﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public class ModAssignment : Block, IAssignmentOperator
    {
        public void Assign<T>(IValue<T> variable, T? value) where T : struct
        {
            variable.Value = (dynamic)variable.Value % value;
        }
    }
}
