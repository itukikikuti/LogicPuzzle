﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace windblow.Drangeon
{
    public abstract class Block : MonoBehaviour
    {
        protected static Stack stack = null;

        [SerializeField] protected Text text = null;
        [SerializeField] protected AudioClip sound = null;
        [HideInInspector] public bool isFixed = false;
        [HideInInspector] public bool isRunning = false;
        private Vector3 pressure;
        private Vector3 targetPosition;

        protected virtual void Start()
        {
            targetPosition = transform.position;
        }

        protected virtual void Update()
        {
            if (isRunning)
            {
                text.color = new Color(1f, 1f, 1f, 1f);
            }
            else
            {
                text.color = new Color(0f, 0f, 0f, 0.5f);
            }

            GameObject fixing = transform.GetChild(transform.childCount - 1).gameObject;
            if (isFixed)
            {
                if (!fixing.activeSelf)
                    fixing.SetActive(true);
            }
            else
            {
                if (fixing.activeSelf)
                    fixing.SetActive(false);
            }

            pressure *= 0.9f;

            if (Vector3.Distance(transform.position, targetPosition) == 0f  && pressure.sqrMagnitude > 30f)
            {
                if (Mathf.Abs(pressure.x) > Mathf.Abs(pressure.y) && Mathf.Abs(pressure.x) > Mathf.Abs(pressure.z))
                {
                    pressure.y = 0f;
                    pressure.z = 0f;
                }

                if (Mathf.Abs(pressure.y) > Mathf.Abs(pressure.x) && Mathf.Abs(pressure.y) > Mathf.Abs(pressure.z))
                {
                    pressure.x = 0f;
                    pressure.z = 0f;
                }

                if (Mathf.Abs(pressure.z) > Mathf.Abs(pressure.x) && Mathf.Abs(pressure.z) > Mathf.Abs(pressure.y))
                {
                    pressure.x = 0f;
                    pressure.y = 0f;
                }

                TryMove(transform.position + pressure.normalized);
            }

            transform.position = Vector3.MoveTowards(transform.position, targetPosition, 0.07f);
        }

        public bool TryMove(Vector3 position)
        {
            if (isFixed)
                return false;

            position.x = Mathf.Round(position.x);
            position.y = 0f;
            position.z = Mathf.Round(position.z);

            Block block = GetBlock(position);

            if (block == null)
            {
                targetPosition = position;
                AudioSource.PlayClipAtPoint(sound, transform.position);
                return true;
            }

            if (block.isFixed)
                return false;

            Vector3 movement = block.transform.position - transform.position;
            if (block.TryMove(block.transform.position + movement))
            {
                targetPosition = position;
                AudioSource.PlayClipAtPoint(sound, transform.position);
                return true;
            }

            return false;
        }

        public Block GetBlock(Vector3 position)
        {
            Block block = null;
            foreach (Collider collider in Physics.OverlapSphere(position, 0.1f))
            {
                if (collider.tag == "Block")
                    block = collider.GetComponent<Block>();
            }
            return block;
        }

        private void OnCollisionStay(Collision collision)
        {
            foreach (ContactPoint contact in collision.contacts)
            {
                if (contact.otherCollider.tag == "Player")
                {
                    pressure += contact.normal;
                    contact.otherCollider.transform.position += -contact.normal * 0.01f;
                }
            }
        }
    }
}
