﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public class If : Block
    {
        [SerializeField] private bool? value = null;
        [HideInInspector] public Else elseStatement = null;
        [HideInInspector] public EndIf endIfStatement = null;

        public bool? Value
        {
            get => value;
            set => this.value = value;
        }

        public void CollectOtherStatement()
        {
            if (elseStatement != null)
                elseStatement.ifStatement = null;

            elseStatement = null;
            endIfStatement = null;

            Vector3 position = transform.position;
            for (int i = 0; i < 100; i++)
            {
                position += Vector3.back;
                Block block = GetBlock(position);

                if (block is Else elseStatement)
                {
                    this.elseStatement = elseStatement;
                    elseStatement.ifStatement = this;
                }

                if (block is EndIf endIfStatement)
                {
                    this.endIfStatement = endIfStatement;
                    return;
                }
            }

            if (elseStatement != null)
                elseStatement.ifStatement = null;

            elseStatement = null;
            endIfStatement = null;
        }
    }
}
