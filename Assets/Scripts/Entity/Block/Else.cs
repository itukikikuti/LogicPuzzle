﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public class Else : Block
    {
        [HideInInspector] public If ifStatement = null;
    }
}
