﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public class Assignment : Block, IAssignmentOperator
    {
        public void Assign(IntVariable variable, int? value)
        {
            variable.Value = value;
        }

        public void Assign<T>(IValue<T> variable, T? value) where T : struct
        {
            variable.Value = value;
        }
    }
}
