﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public interface IValue
    {
        object Value { get; set; }
    }

    public interface IValue<T> : IValue where T : struct
    {
        new T? Value { get; set; }
    }
}
