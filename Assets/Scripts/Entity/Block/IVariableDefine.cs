﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public interface IVariableDefine : IValue
    {
        string Name { get; set; }
    }
}
