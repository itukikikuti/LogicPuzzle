﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public class Mod : Block, IComputeOperator
    {
        public int? Compute(int? a, int? b)
        {
            return a % b;
        }
    }
}
