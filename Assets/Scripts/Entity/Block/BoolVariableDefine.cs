﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace windblow.Drangeon
{
    public class BoolVariableDefine : Define, IValue<bool>, IVariableDefine
    {
        [SerializeField] private bool? value = null;
        [SerializeField] private Text nameText = null;
        [SerializeField] private Text valueText = null;

        public string Name { get; set; }

        public bool? Value
        {
            get => value;
            set => this.value = value;
        }

        object IValue.Value
        {
            get => value;
            set => this.value = value as bool?;
        }

        protected override void Update()
        {
            base.Update();

            nameText.text = "bool " + Name;
            valueText.text = value == null ? "?" : value.ToString().ToLower();
        }
    }
}
