﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace windblow.Drangeon
{
    public class IntVariableDefine : Define, IValue<int>, IVariableDefine
    {
        [SerializeField] private int? value = null;
        [SerializeField] private Text nameText = null;
        [SerializeField] private Text valueText = null;

        public string Name { get; set; }

        public int? Value
        {
            get => value;
            set => this.value = value;
        }

        object IValue.Value
        {
            get => value;
            set => this.value = value as int?;
        }

        protected override void Update()
        {
            base.Update();

            nameText.text = "int " + Name;
            valueText.text = value == null ? "?" : value.ToString();
        }
    }
}
