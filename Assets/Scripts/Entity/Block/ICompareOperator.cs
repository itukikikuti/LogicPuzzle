﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public interface ICompareOperator
    {
        bool? Compare(int? a, int? b);
    }
}
