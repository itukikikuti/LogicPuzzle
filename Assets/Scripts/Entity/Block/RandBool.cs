﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public class RandBool : Block, IValue<bool>
    {
        [SerializeField] private bool value = default;

        public bool? Value
        {
            get => value;
            set => this.value = value ?? default;
        }

        object IValue.Value
        {
            get => value;
            set => this.value = value as bool? ?? default;
        }

        protected override void Update()
        {
            base.Update();

            value = Random.Range(0, 2) == 0;
            text.text = value.ToString().ToLower();
        }
    }
}
