﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public interface IComputeOperator
    {
        int? Compute(int? a, int? b);
    }
}
