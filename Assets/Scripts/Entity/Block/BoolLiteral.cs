﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public class BoolLiteral : Block, IValue<bool>
    {
        [SerializeField] private bool value = default;

        public bool? Value
        {
            get => value;
            set => this.value = value ?? default;
        }

        object IValue.Value
        {
            get => value;
            set => this.value = value as bool? ?? default;
        }

        protected override void Update()
        {
            base.Update();

            text.text = value.ToString().ToLower();
        }
    }
}
