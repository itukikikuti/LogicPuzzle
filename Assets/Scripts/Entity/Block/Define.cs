﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public class Define : Block
    {
        protected override void Start()
        {
            base.Start();

            isFixed = true;
        }
    }
}
