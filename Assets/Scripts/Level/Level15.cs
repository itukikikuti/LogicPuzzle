﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public partial class Level : MonoBehaviour
    {
        private Data CreateData15()
        {
            Data data = new Data();

            data.size = new Vector2Int(5, 5);

            data.variables.Add(("a", typeof(int)));
            data.variables.Add(("b", typeof(int)));
            data.offset += 1;

            int a = random.Next(0, 10);
            int b = random.Next(0, 10);

            if (a == b)
                b = random.Next(0, 10);

            data.requireValue = x =>
            {
                return ((int?)x[0] >= 50 && (int?)x[1] == a) || ((int?)x[0] < 50 && (int?)x[1] == b);
            };

            data.AddFixBlock(new Vector3Int(0, 0, 8), "a");
            data.AddFixBlock(new Vector3Int(1, 0, 8), "=");
            data.AddFixBlock(new Vector3Int(2, 0, 8), "randint");
            data.AddBlock(random, "if");
            data.AddBlock(random, "a");
            data.AddBlock(random, ">=");
            data.AddBlock(random, "50");
            data.AddBlock(random, "b");
            data.AddBlock(random, "=");
            data.AddBlock(random, a.ToString());
            data.AddBlock(random, "b");
            data.AddBlock(random, "=");
            data.AddBlock(random, b.ToString());
            data.AddBlock(random, "else");
            data.AddBlock(random, "endif");

            data.missionText = string.Format("変数aが50以上の時は、変数bを{0}に\nそれ以外の時は、変数bを{1}にしよう", a, b);

            return data;
        }
    }
}
