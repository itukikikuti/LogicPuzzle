﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public partial class Level : MonoBehaviour
    {
        private Data CreateData1()
        {
            Data data = new Data();

            int variableCount = random.Next(1, 3);

            data.size = new Vector2Int(5, 2);

            int[] values = new int[variableCount];

            for (int i = 0; i < variableCount; i++)
            {
                string name = ((char)('a' + i)).ToString();
                values[i] = random.Next(0, 100);

                data.variables.Add((name, typeof(int)));

                data.AddBlock(random, name);
                data.AddBlock(random, "=");
                data.AddBlock(random, values[i].ToString());

                data.missionText += string.Format("変数{0}を{1}にしよう\n", name, values[i]);
            }

            data.requireValue = v =>
            {
                for (int i = 0; i < variableCount; i++)
                {
                    if ((int?)v[i] != values[i])
                        return false;
                }
                return true;
            };

            return data;
        }
    }
}
