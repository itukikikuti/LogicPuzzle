﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public partial class Level : MonoBehaviour
    {
        private Data CreateData4()
        {
            Data data = new Data();

            data.size = new Vector2Int(5, 1);

            int value = -random.Next(1, 10);
            string name = 'a'.ToString();
            data.variables.Add((name, typeof(int)));
            data.requireValue = v => (int?)v[0] == value;

            data.AddBlock(random, name);
            data.AddBlock(random, "=");
            data.AddBlock(random, "-");
            data.AddBlock(random, Mathf.Abs(value).ToString());

            data.missionText = string.Format("変数{0}を{1}にしよう\n", name, value);

            return data;
        }
    }
}
