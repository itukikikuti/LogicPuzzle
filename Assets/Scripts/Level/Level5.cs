﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public partial class Level : MonoBehaviour
    {
        private Data CreateData5()
        {
            Data data = new Data();

            data.size = new Vector2Int(5, 2);

            int a = -random.Next(1, 10);
            int b = -a;
            data.variables.Add((nameof(a), typeof(int)));
            data.variables.Add((nameof(b), typeof(int)));
            data.requireValue = v => (int?)v[0] == a;
            data.requireValue = v => (int?)v[1] == b;

            data.AddBlock(random, nameof(a));
            data.AddBlock(random, nameof(a));
            data.AddBlock(random, nameof(b));
            data.AddBlock(random, "=");
            data.AddBlock(random, "=");
            data.AddBlock(random, "-");
            data.AddBlock(random, "-");
            data.AddBlock(random, Mathf.Abs(a).ToString());

            data.missionText += string.Format("変数{0}を{1}にしよう\n", nameof(a), a);
            data.missionText += string.Format("変数{0}を{1}にしよう\n", nameof(b), b);

            return data;
        }
    }
}
