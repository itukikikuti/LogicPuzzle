﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public partial class Level : MonoBehaviour
    {
        private Data CreateData14()
        {
            Data data = new Data();

            data.size = new Vector2Int(5, 4);

            data.variables.Add(("a", typeof(bool)));
            data.variables.Add(("b", typeof(int)));
            data.offset += 1;

            int a = random.Next(0, 10);
            int b = random.Next(0, 10);

            if (a == b)
                b = random.Next(0, 10);

            data.requireValue = x =>
            {
                return ((bool?)x[0] == true && (int?)x[1] == a) || ((bool?)x[0] == false && (int?)x[1] == b);
            };

            data.AddFixBlock(new Vector3Int(0, 0, 7), "a");
            data.AddFixBlock(new Vector3Int(1, 0, 7), "=");
            data.AddFixBlock(new Vector3Int(2, 0, 7), "randbool");
            data.AddBlock(random, "if");
            data.AddBlock(random, "a");
            data.AddBlock(random, "b");
            data.AddBlock(random, "=");
            data.AddBlock(random, a.ToString());
            data.AddBlock(random, "b");
            data.AddBlock(random, "=");
            data.AddBlock(random, b.ToString());
            data.AddBlock(random, "else");
            data.AddBlock(random, "endif");

            data.missionText = string.Format("変数aがtrueの時は、変数bを{0}に\nそれ以外の時は、変数bを{1}にしよう", a, b);

            return data;
        }
    }
}
