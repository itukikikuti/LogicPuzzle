﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace windblow.Drangeon
{
    public class Test : Level
    {
        [SerializeField] TextAsset dataAsset = null;
        private Data data = new Data();

        private void Start()
        {
            data.variables.Add(("a", typeof(int)));
            data.variables.Add(("b", typeof(int)));
            data.variables.Add(("c", typeof(int)));
            data.variables.Add(("d", typeof(int)));
            data.variables.Add(("e", typeof(int)));
            data.variables.Add(("f", typeof(int)));
            data.variables.Add(("g", typeof(bool)));
            data.variables.Add(("h", typeof(bool)));
            data.variables.Add(("i", typeof(bool)));
            data.variables.Add(("j", typeof(bool)));
            data.variables.Add(("k", typeof(bool)));
            data.variables.Add(("l", typeof(bool)));
            data.variables.Add(("m", typeof(int)));

            data.requireValue = x =>
            {
                return (int?)x[0] == 6 &&
                    (int?)x[1] == 5 &&
                    (int?)x[2] == 4 &&
                    (int?)x[3] == -4 &&
                    (int?)x[4] == 8 &&
                    (int?)x[5] == 2 &&
                    (bool?)x[6] == true &&
                    (bool?)x[7] == true &&
                    (bool?)x[8] == false &&
                    (bool?)x[9] == false &&
                    (bool?)x[10] == false;
            };

            data.size.y = -2;

            string[] lines = dataAsset.text.Replace("\r\n", "\n").Split('\n');
            for (int y = 0; y < lines.Length; y++)
            {
                string[] tokens = lines[y].Split(' ');
                for (int x = 0; x < tokens.Length; x++)
                {
                    data.blockInfos.Add(tokens[x]);
                    data.blockPositions.Add(new Vector3Int(x, 0, -y));
                    data.blockFixs.Add(false);
                }
            }

            Create(data);
            player.transform.position = new Vector3(-1f, 0f, 0f);
            player.GetComponent<Player>().speed = 6f;
            entryPoint.wait = 50;
        }

        private void Update()
        {
            if (!entryPoint.isRunning && Input.GetKeyDown(KeyCode.Return))
            {
                foreach (var variable in variables)
                {
                    variable.Value.Value = null;
                }
                entryPoint.Run();
            }

            if (Input.GetKey(KeyCode.Escape))
                SceneManager.LoadScene("Select");

            if (data.requireBeepCount > 0)
            {
                if (data.requireBeepCount > entryPoint.beepCallCount)
                    return;
            }

            if (!data.requireValue(variables.Select(x => x.Value.Value).ToArray()))
                return;

            door.Open();
        }
    }
}
