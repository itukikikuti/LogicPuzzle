﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public partial class Level : MonoBehaviour
    {
        private Data CreateData11()
        {
            Data data = new Data();

            data.size = new Vector2Int(5, 1);

            data.requireBeepCount = 1;
            data.offset += 5;

            data.AddFixBlock(new Vector3Int(0, 0, 8), "if");

            bool value = false;
            int a = random.Next(0, 10);
            int b = random.Next(0, 10);

            data.AddFixBlock(new Vector3Int(1, 0, 8), a.ToString());
            data.AddFixBlock(new Vector3Int(3, 0, 8), b.ToString());

            switch (random.Next(6))
            {
                case 0: // ==
                    value = a == b;
                    data.AddFixBlock(new Vector3Int(2, 0, 8), "==");
                    break;
                case 1: // !=
                    value = a != b;
                    data.AddFixBlock(new Vector3Int(2, 0, 8), "!=");
                    break;
                case 2: // >
                    value = a > b;
                    data.AddFixBlock(new Vector3Int(2, 0, 8), ">");
                    break;
                case 3: // >=
                    value = a >= b;
                    data.AddFixBlock(new Vector3Int(2, 0, 8), ">=");
                    break;
                case 4: // <
                    value = a < b;
                    data.AddFixBlock(new Vector3Int(2, 0, 8), "<");
                    break;
                case 5: // <=
                    value = a <= b;
                    data.AddFixBlock(new Vector3Int(2, 0, 8), "<=");
                    break;
            }

            data.AddFixBlock(new Vector3Int(0, 0, 6), "else");
            data.AddFixBlock(new Vector3Int(0, 0, 4), "endif");
            data.AddBlock(random, "beep");

            data.missionText = string.Format("音を{0}回鳴らそう\n", data.requireBeepCount);

            return data;
        }
    }
}
