﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public partial class Level : MonoBehaviour
    {
        private Data CreateData7()
        {
            Data data = new Data();

            int variableCount = random.Next(1, 3);

            bool[] values = new bool[variableCount];
            data.size = new Vector2Int(5, 2);

            for (int i = 0; i < variableCount; i++)
            {
                string name = ((char)('a' + i)).ToString();
                values[i] = random.Next(2) == 0;

                data.variables.Add((name, typeof(bool)));

                data.AddBlock(random, name);
                data.AddBlock(random, "=");
                data.AddBlock(random, values[i].ToString().ToLower());

                data.missionText += string.Format("変数{0}を{1}にしよう\n", name, values[i].ToString().ToLower());
            }

            data.requireValue = v =>
            {
                for (int i = 0; i < variableCount; i++)
                {
                    if ((bool?)v[i] != values[i])
                        return false;
                }
                return true;
            };

            return data;
        }
    }
}
