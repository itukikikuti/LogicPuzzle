﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public partial class Level : MonoBehaviour
    {
        private Data CreateData2()
        {
            Data data = new Data();

            int current = datas.Count;

            data.size = new Vector2Int(5, 3);

            int a = random.Next(1, 10);
            int b = random.Next(1, 10);
            int value = 0;

            if (a < b)
            {
                int temp = a;
                a = b;
                b = temp;
            }

            switch (random.Next(5))
            {
                case 0: // Add
                    value = a + b;
                    data.AddBlock(random, "+");
                    break;
                case 1: // Sub
                    value = a - b;
                    data.AddBlock(random, "-");
                    break;
                case 2: // Mul
                    if (b == 1) b = 2;
                    value = a * b;
                    data.AddBlock(random, "*");
                    break;
                case 3: // Div
                    value = random.Next(1, 10);
                    b = random.Next(2, 10);
                    a = value * b;
                    data.AddBlock(random, "/");
                    break;
                case 4: // Mod
                    value = a % b;
                    data.AddBlock(random, "%");
                    break;
            }

            string name = 'a'.ToString();
            data.variables.Add((name, typeof(int)));
            data.requireValue = v => (int?)v[0] == value;

            data.AddBlock(random, name);
            data.AddBlock(random, "=");
            data.AddBlock(random, a.ToString());
            data.AddBlock(random, b.ToString());

            data.missionText = string.Format("変数{0}を{1}にしよう\n", name, value);

            return data;
        }
    }
}
