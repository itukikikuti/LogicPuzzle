﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public class Door : MonoBehaviour
    {
        [SerializeField] private Transform leftHinge = null;
        [SerializeField] private Transform rightHinge = null;
        [SerializeField] private ParticleSystem particle = null;
        [SerializeField] private AudioClip sound = null;
        [HideInInspector] public Level level = null;
        private bool isOpened = false;

        private void OnTriggerEnter(Collider collider)
        {
            if (collider.tag != "Player")
                return;

            level.Next();
        }

        public void Open()
        {
            if (isOpened)
                return;

            isOpened = true;
            GetComponent<Collider>().isTrigger = true;
            particle.Play();
            AudioSource.PlayClipAtPoint(sound, transform.position);
            StartCoroutine(RunOpen());
        }

        private IEnumerator RunOpen()
        {
            float second = 0.3f;
            float t = 0f;
            while (t < 1f)
            {
                leftHinge.localEulerAngles = new Vector3(0f, Easing.CircularOut(0f, 90f, t), 0f);
                rightHinge.localEulerAngles = new Vector3(0f, Easing.CircularOut(0f, -90f, t), 0f);
                t += Time.deltaTime / second;
                yield return null;
            }

            leftHinge.localEulerAngles = new Vector3(0f, 90f, 0f);
            rightHinge.localEulerAngles = new Vector3(0f, -90f, 0f);
        }
    }
}
