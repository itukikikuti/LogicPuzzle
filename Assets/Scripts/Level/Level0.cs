﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public partial class Level : MonoBehaviour
    {
        private Data CreateData0()
        {
            Data data = new Data();

            data.requireBeepCount = random.Next(1, 4);

            data.size = new Vector2Int(5, 1);

            for (int i = 0; i < data.requireBeepCount; i++)
            {
                data.AddBlock(random, "beep");
            }

            data.missionText = string.Format("音を{0}回鳴らそう\n", data.requireBeepCount);

            return data;
        }
    }
}
