﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public partial class Level : MonoBehaviour
    {
        private Data CreateData8()
        {
            Data data = new Data();

            data.size = new Vector2Int(5, 2);

            bool value = false;
            int a = random.Next(0, 10);
            int b = random.Next(0, 10);

            switch (random.Next(2))
            {
                case 0: // ==
                    value = a == b;
                    data.AddBlock(random, "==");
                    break;
                case 1: // !=
                    value = a != b;
                    data.AddBlock(random, "!=");
                    break;
            }

            data.variables.Add(("a", typeof(bool)));
            data.requireValue = v => (bool?)v[0] == value;

            data.AddBlock(random, "a");
            data.AddBlock(random, "=");
            data.AddBlock(random, a.ToString());
            data.AddBlock(random, b.ToString());

            data.missionText += string.Format("変数{0}を{1}にしよう\n", "a", value.ToString().ToLower());

            return data;
        }
    }
}
