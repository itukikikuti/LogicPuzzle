﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public partial class Level : MonoBehaviour
    {
        private Data CreateData10()
        {
            Data data = new Data();

            data.size = new Vector2Int(5, 1);

            data.requireBeepCount = 1;
            data.offset += 5;

            bool value = random.Next(2) == 0;
            data.AddFixBlock(new Vector3Int(0, 0, 8), "if");
            data.AddFixBlock(new Vector3Int(1, 0, 8), value.ToString().ToLower());
            data.AddFixBlock(new Vector3Int(0, 0, 6), "else");
            data.AddFixBlock(new Vector3Int(0, 0, 4), "endif");
            data.AddBlock(random, "beep");

            data.missionText = string.Format("音を{0}回鳴らそう\n", data.requireBeepCount);

            return data;
        }
    }
}
