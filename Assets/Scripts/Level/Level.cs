﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace windblow.Drangeon
{
    public partial class Level : MonoBehaviour
    {
        public class Data
        {
            public List<(string, Type)> variables = new List<(string, Type)>();
            public int requireBeepCount = 0;
            public Func<object[], bool> requireValue = v => true;
            public string missionText = "";
            public Vector2Int size;
            public int offset = 3;

            public List<string> blockInfos = new List<string>();
            public List<Vector3Int> blockPositions = new List<Vector3Int>();
            public List<bool> blockFixs = new List<bool>();

            public void AddBlock(System.Random random, string blockInfo)
            {
                blockInfos.Add(blockInfo);

                int temp = (size.x - 1) / 2;

                Vector3Int position;
                do
                {
                    position = new Vector3Int(random.Next(-temp, temp + 1), 0, random.Next(2, 2 + size.y));
                } while (blockPositions.Contains(position));

                blockPositions.Add(position);
                blockFixs.Add(false);
            }

            public void AddFixBlock(Vector3Int position, string blockInfo)
            {
                blockInfos.Add(blockInfo);
                blockPositions.Add(position);
                blockFixs.Add(true);
            }
        }

        [SerializeField] protected int level = -1;
        [SerializeField] protected int floorLength = 10;
        [SerializeField] protected int seed = 0;
        [SerializeField] protected Transform player = null;
        [SerializeField] protected Text text = null;
        [SerializeField] protected GameObject doorPrefab = null;
        [SerializeField] protected GameObject entryPointPrefab = null;
        [SerializeField] protected GameObject intVariableDefinePrefab = null;
        [SerializeField] protected GameObject boolVariableDefinePrefab = null;
        [SerializeField] protected GameObject intVariablePrefab = null;
        [SerializeField] protected GameObject boolVariablePrefab = null;
        [SerializeField] protected GameObject intLiteralPrefab = null;
        [SerializeField] protected GameObject boolLiteralPrefab = null;
        [SerializeField] protected GameObject beepPrefab = null;
        [SerializeField] protected GameObject randIntPrefab = null;
        [SerializeField] protected GameObject randBoolPrefab = null;
        [SerializeField] protected GameObject assignOperatorPrefab = null;
        [SerializeField] protected GameObject addAssignOperatorPrefab = null;
        [SerializeField] protected GameObject subAssignOperatorPrefab = null;
        [SerializeField] protected GameObject mulAssignOperatorPrefab = null;
        [SerializeField] protected GameObject divAssignOperatorPrefab = null;
        [SerializeField] protected GameObject modAssignOperatorPrefab = null;
        [SerializeField] protected GameObject incrementPrefab = null;
        [SerializeField] protected GameObject decrementPrefab = null;
        [SerializeField] protected GameObject addPrefab = null;
        [SerializeField] protected GameObject subPrefab = null;
        [SerializeField] protected GameObject mulPrefab = null;
        [SerializeField] protected GameObject divPrefab = null;
        [SerializeField] protected GameObject modPrefab = null;
        [SerializeField] protected GameObject equalPrefab = null;
        [SerializeField] protected GameObject notEqualPrefab = null;
        [SerializeField] protected GameObject greaterPrefab = null;
        [SerializeField] protected GameObject greaterEqualPrefab = null;
        [SerializeField] protected GameObject lessPrefab = null;
        [SerializeField] protected GameObject lessEqualPrefab = null;
        [SerializeField] protected GameObject ifPrefab = null;
        [SerializeField] protected GameObject elsePrefab = null;
        [SerializeField] protected GameObject endIfPrefab = null;
        protected System.Random random;
        protected List<Data> datas = new List<Data>();
        protected int currentFloor = 0;
        protected EntryPoint entryPoint = null;
        protected Door door = null;
        protected Dictionary<string, IVariableDefine> variables = new Dictionary<string, IVariableDefine>();
        private Func<Data>[] CreateData;

        private Data GetData(int floor)
        {
            while (datas.Count < floor + 1)
            {
                int temp = level;

                if (temp < 0)
                    temp = random.Next(CreateData.Length);

                datas.Add(CreateData[temp]());
            }

            return datas[floor];
        }

        public void Create(Data data)
        {
            player.transform.position = Vector3.zero;

            foreach (Transform child in transform)
            {
                Destroy(child.gameObject);
            }

            GameObject clone = Instantiate(doorPrefab, transform);
            clone.transform.position = new Vector3(0f, 0f, data.size.y + data.offset + 3f);
            clone.transform.eulerAngles = new Vector3(0f, 180f, 0f);
            door = clone.GetComponent<Door>();
            door.level = this;

            clone = Instantiate(entryPointPrefab, transform);
            clone.transform.position = new Vector3(0f, 0f, data.size.y + data.offset);
            entryPoint = clone.GetComponent<EntryPoint>();

            variables.Clear();

            for (int i = 0; i < data.variables.Count; i++)
            {
                if (data.variables[i].Item2 == typeof(int))
                    clone = Instantiate(intVariableDefinePrefab, transform);

                if (data.variables[i].Item2 == typeof(bool))
                    clone = Instantiate(boolVariableDefinePrefab, transform);

                clone.transform.position = new Vector3(-5f, 0f, data.size.y + data.offset - i);
                IVariableDefine temp = clone.GetComponent<IVariableDefine>();
                temp.Name = data.variables[i].Item1;
                variables[temp.Name] = temp;
            }

            for (int i = 0; i < data.blockInfos.Count; i++)
            {
                string blockInfo = data.blockInfos[i];
                Vector3Int blockPosition = data.blockPositions[i];

                if (blockInfo == "beep")
                    clone = Instantiate(beepPrefab, transform);

                if (blockInfo == "randint")
                    clone = Instantiate(randIntPrefab, transform);

                if (blockInfo == "randbool")
                    clone = Instantiate(randBoolPrefab, transform);

                if (Regex.IsMatch(blockInfo, "^[a-z]$"))
                {
                    if (variables[blockInfo] is IntVariableDefine)
                        clone = Instantiate(intVariablePrefab, transform);

                    if (variables[blockInfo] is BoolVariableDefine)
                        clone = Instantiate(boolVariablePrefab, transform);

                    IVariable temp = clone.GetComponent<IVariable>();
                    temp.Name = blockInfo;
                    temp.Define = variables[blockInfo];
                }

                if (blockInfo == "=")
                    clone = Instantiate(assignOperatorPrefab, transform);

                if (blockInfo == "+=")
                    clone = Instantiate(addAssignOperatorPrefab, transform);

                if (blockInfo == "-=")
                    clone = Instantiate(subAssignOperatorPrefab, transform);

                if (blockInfo == "*=")
                    clone = Instantiate(mulAssignOperatorPrefab, transform);

                if (blockInfo == "/=")
                    clone = Instantiate(divAssignOperatorPrefab, transform);

                if (blockInfo == "%=")
                    clone = Instantiate(modAssignOperatorPrefab, transform);

                if (blockInfo == "++")
                    clone = Instantiate(incrementPrefab, transform);

                if (blockInfo == "--")
                    clone = Instantiate(decrementPrefab, transform);

                if (Regex.IsMatch(blockInfo, "[0-9]+"))
                {
                    clone = Instantiate(intLiteralPrefab, transform);
                    IntLiteral temp = clone.GetComponent<IntLiteral>();
                    temp.Value = System.Convert.ToInt32(blockInfo);
                }

                if (blockInfo == "true" || blockInfo == "false")
                {
                    clone = Instantiate(boolLiteralPrefab, transform);
                    BoolLiteral temp = clone.GetComponent<BoolLiteral>();
                    temp.Value = System.Convert.ToBoolean(blockInfo);
                }

                if (blockInfo == "+")
                    clone = Instantiate(addPrefab, transform);

                if (blockInfo == "-")
                    clone = Instantiate(subPrefab, transform);

                if (blockInfo == "*")
                    clone = Instantiate(mulPrefab, transform);

                if (blockInfo == "/")
                    clone = Instantiate(divPrefab, transform);

                if (blockInfo == "%")
                    clone = Instantiate(modPrefab, transform);

                if (blockInfo == "==")
                    clone = Instantiate(equalPrefab, transform);

                if (blockInfo == "!=")
                    clone = Instantiate(notEqualPrefab, transform);

                if (blockInfo == ">")
                    clone = Instantiate(greaterPrefab, transform);

                if (blockInfo == ">=")
                    clone = Instantiate(greaterEqualPrefab, transform);

                if (blockInfo == "<")
                    clone = Instantiate(lessPrefab, transform);

                if (blockInfo == "<=")
                    clone = Instantiate(lessEqualPrefab, transform);

                if (blockInfo == "if")
                    clone = Instantiate(ifPrefab, transform);

                if (blockInfo == "else")
                    clone = Instantiate(elsePrefab, transform);

                if (blockInfo == "endif")
                    clone = Instantiate(endIfPrefab, transform);

                clone.transform.position = blockPosition;
                clone.GetComponent<Block>().isFixed = data.blockFixs[i];
            }

            if (text != null)
                text.text = data.missionText;
        }

        private void Start()
        {
            CreateData = new Func<Data>[]
            {
                CreateData0,
                CreateData1,
                CreateData2,
                CreateData3,
                CreateData4,
                CreateData5,
                CreateData6,
                CreateData7,
                CreateData8,
                CreateData9,
                CreateData10,
                CreateData11,
                CreateData12,
                CreateData13,
                CreateData14,
                CreateData15,
            };

            if (level < 0)
                random = new System.Random();
            else
                random = new System.Random(seed);

            Create(GetData(currentFloor));
        }

        private void Update()
        {
            if (!entryPoint.isRunning && Input.GetKeyDown(KeyCode.Return))
            {
                foreach (var variable in variables)
                {
                    variable.Value.Value = null;
                }
                entryPoint.Run();
            }

            // 裏コマンド
            if (Input.GetKey(KeyCode.I) && Input.GetKey(KeyCode.K))
                door.Open();

            if (Input.GetKey(KeyCode.Escape))
                SceneManager.LoadScene("Select");

            Data data = GetData(currentFloor);

            if (data.requireBeepCount > 0)
            {
                if (data.requireBeepCount > entryPoint.beepCallCount)
                    return;
            }

            if (!data.requireValue(variables.Select(x => x.Value.Value).ToArray()))
                return;

            door.Open();
        }

        public void Next()
        {
            if (level < 0 || currentFloor < floorLength - 1)
            {
                currentFloor++;
                Create(GetData(currentFloor));
            }
            else
            {
                SceneManager.LoadScene("Select");
            }
        }
    }
}
