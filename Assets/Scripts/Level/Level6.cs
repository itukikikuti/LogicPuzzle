﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow.Drangeon
{
    public partial class Level : MonoBehaviour
    {
        private Data CreateData6()
        {
            Data data = new Data();

            data.size = new Vector2Int(5, 2);

            int a = random.Next(1, 10);
            data.variables.Add((nameof(a), typeof(int)));
            data.requireValue = v => (int?)v[0] == a;

            data.AddBlock(random, nameof(a));
            data.AddBlock(random, nameof(a));
            data.AddBlock(random, "=");

            switch (random.Next(2))
            {
                case 0: // ++
                    data.AddBlock(random, "++");
                    data.AddBlock(random, (a - 1).ToString());
                    break;
                case 1: // --
                    data.AddBlock(random, "--");
                    data.AddBlock(random, (a + 1).ToString());
                    break;
            }

            data.missionText += string.Format("変数{0}を{1}にしよう\n", nameof(a), a);

            return data;
        }
    }
}
