﻿using System;
using System.Collections;
using UnityEngine;

namespace windblow
{
    public static class Utility
    {
        private class Coroutiner : MonoBehaviour
        {
            public IEnumerator Run(IEnumerator coroutine)
            {
                yield return coroutine;
                Destroy(gameObject);
            }
        }

        public static void StartLambda(Action lambda)
        {
            lambda();
        }

        public static Coroutine StartCoroutine(IEnumerator coroutine)
        {
            GameObject go = new GameObject("One shot coroutine");
            Coroutiner coroutiner = go.AddComponent<Coroutiner>();
            return coroutiner.StartCoroutine(coroutiner.Run(coroutine));
        }

        public static Vector2 Bezier(Vector2 a, Vector2 control, Vector2 b, float t)
        {
            return new Vector2(
                (1f - t) * (1f - t) * a.x + 2f * (1f - t) * t * control.x + t * t * b.x,
                (1f - t) * (1f - t) * a.y + 2f * (1f - t) * t * control.y + t * t * b.y
            );
        }

        public static Vector3 Bezier(Vector3 a, Vector3 control, Vector3 b, float t)
        {
            return new Vector3(
                (1f - t) * (1f - t) * a.x + 2f * (1f - t) * t * control.x + t * t * b.x,
                (1f - t) * (1f - t) * a.y + 2f * (1f - t) * t * control.y + t * t * b.y,
                (1f - t) * (1f - t) * a.z + 2f * (1f - t) * t * control.z + t * t * b.z
            );
        }
    }

    public enum Orientation
    {
        Up,
        Right,
        Down,
        Left
    }
}
