﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow
{
    public static class Easing
    {
        public static float CircularIn(float a, float b, float t)
        {
            float c = b - a;
            return -c * (Mathf.Sqrt(1f - t * t) - 1f) + a;
        }

        public static float CircularOut(float a, float b, float t)
        {
            float c = b - a;
            t = t - 1f;
            return c * Mathf.Sqrt(1f - t * t) + a;
        }
    }
}
