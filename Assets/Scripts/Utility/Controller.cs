﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace windblow
{
    [RequireComponent(typeof(Rigidbody))]
    public class Controller : MonoBehaviour
    {
        private new Rigidbody rigidbody = null;

        private void Start()
        {
            rigidbody = GetComponent<Rigidbody>();
        }

        public void SimpleMove(Vector3 velocity)
        {
            rigidbody.velocity = new Vector3(velocity.x, rigidbody.velocity.y, velocity.z);
        }
    }
}
